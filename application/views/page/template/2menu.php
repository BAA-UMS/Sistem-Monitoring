<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part" style="">
                <!-- Logo -->
                <a class="logo" href="<?php echo base_url() ?>">
                    <!-- Logo icon image, you can use font-icon also -->
                    <div style="float:left" >
                        <b>
                            <!--This is dark logo icon-->
                            <!-- <img src="https://wrappixel.com/ampleadmin/ampleadmin-html/plugins/images/admin-logo.png"
                                 alt="home" class="dark-logo"/> -->
                            <!--This is light logo icon-->
                            <img src="<?php echo base_url('assets/images/system/ums-icon.png') ?>"
                                 alt="home" class="light-logo"/>
                        </b>
                    </div>
                    <div style="float: right">
                        <!-- Logo text image you can use text also -->
                        <!-- <span style="color: black; font-weight: bold;">Sistem</span>
                        <span style="color: black">Monitoring</span> -->
                        <span class="hidden-xs">
                            <b style="color: #000;font-size: 16px;line-height: 15px; padding: 20px 30px 0 0">
                                Pangkalan Data <br> MHS UMS
                            </b>
                            <!-- This is dark logo text -->
                            <!-- <img src="https://wrappixel.com/ampleadmin/ampleadmin-html/plugins/images/admin-text.png"
                                 alt="home" class="dark-logo"/> -->
                            <!-- This is light logo text -->
                            <!-- <img src="https://wrappixel.com/ampleadmin/ampleadmin-html/plugins/images/admin-text-dark.png"
                                 alt="home" class="light-logo"/> -->
                         </span>
                    </div>
                </a>
            </div>
            <!-- /Logo -->
            <!-- Search input and Toggle icon -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a>
                </li>

            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li>
                    <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                        <input type="text" placeholder="Search..." class="form-control"> <a href="#"><i
                                    class="fa fa-search"></i></a></form>
                </li>
                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->


    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav slimscrollsidebar">
            <div class="sidebar-head">
                <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigasi</span>
                </h3>
            </div>
            <div class="user-profile">
                <!-- <div class="dropdown user-pro-body">
                    <div>
                        <img src="https://wrappixel.com/ampleadmin/ampleadmin-html/plugins/images/users/varun.jpg"
                              alt="user-img" class="img-circle">
                    </div>
                    <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">Steave Gection <span class="caret"></span></a>
                    <ul class="dropdown-menu animated flipInY">
                        <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                        <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                        <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                </div> -->
            </div>

            <ul class="nav" id="side-menu">
                <li>
                    <a href="<?php echo base_url('aww') ?>" class="waves-effect">
                        <i class="fa fa-users fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Mahasiswa <span class="fa arrow"></span> </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <!-- <li>
                            <a href="<?php echo base_url('monitoring/mahasiswa/pendaftar') ?>">
                                <i class="fa fa-user fa-fw"></i>
                                <span class="hide-menu">Pendaftar</span>
                            </a>
                        </li> -->
                        <li>
                            <a href="<?php echo base_url('monitoring/mahasiswa/nilai-aik') ?>">
                                <i class="fa fa-user fa-fw"></i>
                                <span class="hide-menu">Mhs Nilai AIK</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('monitoring/mahasiswa/per-angkatan') ?>">
                                <i class="fa fa-user fa-fw"></i>
                                <span class="hide-menu">Mhs Per Angkatan</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('monitoring/mahasiswa/masuk-dan-lulus') ?>">
                                <i class="fa fa-user fa-fw"></i>
                                <span class="hide-menu">Mhs Masuk dan Lulus</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- <li>
                    <a href="<?php echo base_url('monitoring/sad') ?>" class="waves-effect">
                        <i class="fa fa-bar-chart-o fa-fw"></i>
                        <span class="hide-menu">Menu 2</span>
                    </a>
                </li> -->
            </ul>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <div id="monitoring">