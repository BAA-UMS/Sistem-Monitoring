<?php $this->load->view('page/template/1head') ?>
<?php $this->load->view('page/template/2menu') ?>

    <!-- content -->
	<div id="page-wrapper">
	    <div class="container-fluid">
	        <div class="row bg-title">
	            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	                <h4 class="page-title"><?php echo $title ?></h4></div>
	            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
	                <!-- <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button> -->
	                <!-- <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a> -->
	                <ol class="breadcrumb">
	                    <li><a href="#">Mahasiswa</a></li>
	                    <li class="active">Nilai AIK</li>
	                </ol>
	            </div>
	        </div>
	        <!-- ============================================================== -->

	        <!-- .row -->
	        <mhs-nilai-aik></mhs-nilai-aik>
	        <!-- /.row -->
	    </div>
	</div>
    <!-- endcontent -->
<?php $this->load->view('page/template/3foot') ?>