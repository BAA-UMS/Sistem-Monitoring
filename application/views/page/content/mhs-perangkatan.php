<?php $this->load->view('page/template/1head') ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>


<?php $this->load->view('page/template/2menu') ?>


    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title"><?php echo $title ?></h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <!-- <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button> -->
                    <!-- <a href="javascript: void(0);" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a> -->
                    <ol class="breadcrumb">
                        <li><a href="#">Mahasiswa</a></li>
                        <li class="active">Per Angkatan</li>
                    </ol>
                </div>
            </div>
            <!-- ============================================================== -->

            <!-- .row -->
            <mhs-per-angkatan></mhs-per-angkatan>
            <!-- /.row -->
        </div>
    </div>

<?php $this->load->view('page/template/3foot') ?>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script> -->