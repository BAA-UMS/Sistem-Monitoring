/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

// Lib import
import Vue from 'vue'
import { Line } from 'vue-chartjs'
import VueToasted from 'vue-toasted'
import Popover  from 'vue-js-popover'
import ToggleButton from 'vue-js-toggle-button'
import vueSmoothScroll from 'vue-smooth-scroll'
// import BootstrapVue from 'bootstrap-vue'

let toastedOptions = {
    iconPack: 'fontawesome'
}

Vue.use(Popover)
Vue.use(ToggleButton)
Vue.use(vueSmoothScroll)
Vue.use(VueToasted, toastedOptions)
// Vue.use(BootstrapVue);


// require('bootstrap/dist/css/bootstrap.min.css');
// require('bootstrap/dist/js/bootstrap.min.js');
// require('bootstrap-select/dist/css/bootstrap-select.min.css');
// require('bootstrap-select/dist/js/bootstrap-select.min.js');

Vue.component('mhs-per-angkatan', require('./components/mahasiswa/mhsPerAngkatan.vue'))
Vue.component('mhs-pendaftar', require('./components/mahasiswa/mhsPendaftar.vue'))
Vue.component('mhs-masuk-dan-lulus', require('./components/mahasiswa/mhsMasukDanLulus.vue'))
Vue.component('mhs-nilai-aik',require('./components/mahasiswa/mhsNilaiAIK.vue'))

const app = new Vue({
    el: '#monitoring'
});