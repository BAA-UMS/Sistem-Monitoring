<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


//============================== VIEW ==================================//
// Mahasiswa
$route['monitoring/mahasiswa/pendaftar']['get'] = 'view/mahasiswa/Mhs_pendaftar';
$route['monitoring/mahasiswa/per-angkatan']['get'] = 'view/mahasiswa/Mhs_perangkatan';
$route['monitoring/mahasiswa/nilai-aik']['get'] = 'view/mahasiswa/Mhs_nilaiaik';
$route['monitoring/mahasiswa/ums']['get'] = 'view/mahasiswa/Mhs_ums';
$route['monitoring/mahasiswa/masuk-dan-lulus']['get'] = 'view/mahasiswa/Mhs_masukdanlulus';


//============================== API ==================================//
# Mhs-Perangkatan
$route['api/get/mhs-angkatan/initial-data']['get'] = 'api/mahasiswa/Mhs_perangkatan/initialData';
$route['api/post/mhs-angkatan/filter']['post'] = 'api/mahasiswa/Mhs_perangkatan/filterData';
$route['api/get/mhs-angkatan/show-detail-data']['post'] = 'api/mahasiswa/Mhs_perangkatan/showDetailData';
$route['api/get/mhs-angkatan/show-detail-lulusan']['post'] = 'api/mahasiswa/Mhs_perangkatan/showDetailLulusan';
# Mhs-MasukDanLulus
$route['api/get/mhs-masuk-dan-lulus/initial-data']['get'] = 'api/mahasiswa/Mhs_masukdanlulus/initialData';
$route['api/post/mhs-masuk-dan-lulus/filter']['post'] = 'api/mahasiswa/Mhs_masukdanlulus/filterData';
$route['api/get/mhs-masuk-dan-lulus/show-detail-data']['post'] = 'api/mahasiswa/Mhs_masukdanlulus/showDetailData';

$route['api/get/mhs-nilai-aik/initial-data']['get'] = 'api/mahasiswa/Mhs_nilaiaik/initialData';
$route['api/post/mhs-nilai-aik/filter']['post'] = 'api/mahasiswa/Mhs_nilaiaik/filterData';

