<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhs_perangkatan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data = [
            'title' => 'Mahasiswa Per Angkatan'
        ];
        $this->load->view('page/content/mhs-perangkatan', $data);
    }    

}