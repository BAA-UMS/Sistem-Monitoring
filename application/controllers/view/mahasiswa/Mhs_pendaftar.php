<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhs_pendaftar extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data = [
            'title' => 'Pendaftar UMS'
        ];
        $this->load->view('page/content/mhs-pendaftar', $data);
    }


}