<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhs_nilaiaik extends CI_Controller 
{

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data = [
            'title' => 'Nilai AIK'
        ];
        $this->load->view('page/content/mhs-nilaiaik', $data);
    }

}

/* End of file Mhs_nilaiAIK.php */
/* Location: ./application/controllers/view/mahasiswa/Mhs_nilaiAIK.php */