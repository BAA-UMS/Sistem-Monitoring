<?php

class Mhs_masukdanlulus extends CI_Controller
{
    public function initialData()
    {
        $result = [
            'success' => true,
            'result' => [
                "table_data" => [
                    'tahun-4' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]

                    ],
                    'tahun-3' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '900',
                            'lulus_seleksi' => '525'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '412',
                            'transfer' => '24'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '525',
                            'transfer' => '12'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '521',
                            'transfer' => '12'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.3',
                            'rat' => '3.1',
                            'max' => '3.6'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '12', // < 2.75
                            'B' => '95', // 2.76 - 3.5
                            'C' => '51' // > 3.5
                        ]
                    ],
                    'tahun-2' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ],
                    'tahun-1' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ],
                    'tahun' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ]
                ],
                'prodi' => [
                    ['id' => 'IN', 'ket' => 'Informatika'],
                    ['id' => 'KO', 'ket' => 'Komunikasi'],
                    ['id' => 'TS', 'ket' => 'Teknik Sipil'],
                    ['id' => 'KE', 'ket' => 'Kedokteran'],
                    ['id' => 'PE', 'ket' => 'Perawat']
                ],
                'year' => [
                    '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'
                ]
            ]
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }
    public function filterData()
    {
        $data = ($this->input->post());
        // print_r($data);exit();
        if ($data['year']=='2017') {
            $result = [
            'success' => true,
            'result' => [
                    'tahun-4' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]

                    ],
                    'tahun-3' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '900',
                            'lulus_seleksi' => '525'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '412',
                            'transfer' => '24'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '525',
                            'transfer' => '12'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '521',
                            'transfer' => '12'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.3',
                            'rat' => '3.1',
                            'max' => '3.6'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '12', // < 2.75
                            'B' => '95', // 2.76 - 3.5
                            'C' => '51' // > 3.5
                        ]
                    ],
                    'tahun-2' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ],
                    'tahun-1' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ],
                    'tahun' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ]
            ]];
        }else{
            $result = [
            'success' => true,
            'result' => [
                    'tahun-4' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]

                    ],
                    'tahun-3' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '900',
                            'lulus_seleksi' => '525'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '412',
                            'transfer' => '24'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '525',
                            'transfer' => '12'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '521',
                            'transfer' => '12'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.3',
                            'rat' => '3.1',
                            'max' => '3.6'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '12', // < 2.75
                            'B' => '95', // 2.76 - 3.5
                            'C' => '51' // > 3.5
                        ]
                    ],
                    'tahun-2' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ],
                    'tahun-1' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ],
                    'tahun' => [
                        'data_tampung' => '',
                        'jml_calon_mhs_baru' => [
                            'ikut_seleksi'  => '561',
                            'lulus_seleksi' => '500'
                        ],
                        'jml_mhs_baru' => [
                            'reguler'  => '182',
                            'transfer' => '42'
                        ],
                        'jml_total_mhs' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'jml_lulusan' => [
                            'reguler'  => '712',
                            'transfer' => '62'
                        ],
                        'ipk_lulusan_reg' => [
                            'min' => '2.4',
                            'rat' => '3.2',
                            'max' => '3.9'
                        ],
                        'presentase_ipk_lulusan_reg' => [
                            'A' => '17', // < 2.75
                            'B' => '99', // 2.76 - 3.5
                            'C' => '52' // > 3.5
                        ]
                    ]
            ]];
        }
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }
    public function showDetailData()
    {
        $input = $this->input->post();
        // print_r($input);die();
        /*
        Isi variabel Input
        [prodi] => All
        [angkatan] => 2012
        [status_mhs] => masuk  // masuk, masuk_mhs, lulus
        */

        // dump sekolah
        $sekolah= ['SMA', "SMK"];
        function randomsekolah($panjang = 6) {
            $karakter = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $panjangkarakter = strlen($karakter);
            $sekolahrandom = '';
            for ($i = 0; $i < $panjang; $i++) {
                $sekolahrandom .= $karakter[rand(0, $panjangkarakter - 1)];
            }
            return $sekolahrandom;
        }
        
        // Jumalah Calon Mahasiswa
        if ($input['status_mhs'] == 'masuk' ) {
            for ($i=0; $i < 900 ; $i++) { 
                $random = array_rand($sekolah, 1);
                $tmp = [
                    'no_test'  => rand(1,2202),
                    'nama' => 'Aldino' . $i,
                    'asal_sekolah'  => $sekolah[$random] .' '. randomsekolah(),
                ];
                $resultData[] = $tmp;
            };
        }
        // Jumalah Mahasiswa (Total & Baru)
        else if ($input['status_mhs'] == 'masuk_mhs' ) {
            for ($i=0; $i < 500 ; $i++) {
                $random = array_rand($sekolah, 1);
                $tmp = [
                    'nim' => 'L20014' . $i,
                    'nama'  => 'nama' . $i,
                    'asal_sekolah'  => $sekolah[$random] .' '. randomsekolah(). 'asd',
                ];
                $resultData[] = $tmp;
            };
        }
        // Jumalah Lulusan
        else if ($input['status_mhs'] == 'lulus' ) {
            for ($i=0; $i < 300 ; $i++) { 
                $tmp = [
                    'nama' => 'Aldino' . $i,
                    'nim'  => 'L20014' . $i,
                    'ipk'  => '3.' . $i,
                ];
                $resultData[] = $tmp;
            };
        }

        $result = [
            'success' => true,
            'result'  => $resultData
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }
    
}


