<?php

class Mhs_nilaiaik extends CI_Controller
{
    public function initialData()
    {
        $result = [
            'success' => true,
            'result' => [
                // 'data'	=> [
                // 	['makul'=>'agama',
                // 	'nilai'=>[
                // 		'<2,75' 	=> 32,
                // 		'2,75-3,5' 	=> 20,
                // 		'>3,5' 		=> 15,
                // 	]],
                // 	['makul'=>'agama1',
                // 	'nilai'=>[
                // 		'<2,75' 	=> 20,
                // 		'2,75-3,5' 	=> 28,
                // 		'>3,5' 		=> 6,
                // 	]],
                // 	['makul'=>'agama2',
                // 	'nilai'=>[
                // 		'<2,75' 	=> 20,
                // 		'2,75-3,5' 	=> 12,
                // 		'>3,5' 		=> 10,
                // 	]],
                // 	['makul'=>'agama3',
                // 	'nilai'=>[
                // 		'<2,75' 	=> 30,
                // 		'2,75-3,5' 	=> 17,
                // 		'>3,5' 		=> 10,
                // 	]],

                // ],
                'data'	=> [
                	[	'makul'=>'agama',
                		'nilai'=>[32,20,15]
                	],
                	[	'makul'=>'agama1',
                		'nilai'=>[20,28,6]
                	],
                	[	'makul'=>'agama2',
                		'nilai'=>[20,12,10]
                	],
                	[	'makul'=>'agama3',
                		'nilai'=>[30,17,12]
                	]

                ],
                'prodi' => [
                    ['id' => 'IN', 'ket' => 'Informatika'],
                    ['id' => 'KO', 'ket' => 'Komunikasi'],
                    ['id' => 'TS', 'ket' => 'Teknik Sipil'],
                    ['id' => 'KE', 'ket' => 'Kedokteran'],
                    ['id' => 'PE', 'ket' => 'Perawat']
                ],
                'angkatan' => [
                    '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'
                ],
                'matakuliah' => [
                	['kode' => 'a111', 'nama' => 'agama'],
                	['kode' => 'a222', 'nama' => 'agama1'],
                	['kode' => 'a333', 'nama' => 'agama2'],
                	['kode' => 'a444', 'nama' => 'agama3']

                ]
            ]
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }
    public function filterData()
    {
        $data = ($this->input->post());
        // print_r($data);exit();
        if ($data['matakuliah']=='%') {
            $result = [
            'success' => true,
            'result' => [
                    'data'	=> [
                	[	'makul'=>'agama',
                		'nilai'=>[32,12,19]
                	],
                	[	'makul'=>'agama1',
                		'nilai'=>[20,13,30]
                	],
                	[	'makul'=>'agama2',
                		'nilai'=>[27,14,10]
                	],
                	[	'makul'=>'agama3',
                		'nilai'=>[30,17,12]
                	]

                ]
            ]];
        }else{
            $result = [
            'success' => true,
            'result' => [
                    'data'	=> [
                	[	'makul'=>'agama',
                		'nilai'=>[17,9,28]
                	]]
            ]];
        }
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }
}
    