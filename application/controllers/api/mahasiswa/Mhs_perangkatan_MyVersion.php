<?php

// use Restserver\Libraries\REST_Controller;

class Mhs_perangkatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $config = [
                'server'          => 'http://10.3.11.15/restmonitor/baa',
                'api_key'         => 'haunans',
                'api_name'        => 'X-API-KEY',
                'http_user'       => 'admin',
                'http_pass'       => '1234',
                'http_auth'       => 'basic',
                'ssl_verify_peer' => TRUE,
                'ssl_cainfo'      => '/certs/cert.pem'
            ];

        // Run some setup
        $this->rest->initialize($config);
    }


    public function initialData()
    {        
        $tahun = 2016;
        // ===================== GET TABLE DATA ====================== //
        $finalResult = [];
        for ($i=0; $i < 7 ; $i++) { 
            $getData = $this->rest->post('mhsAktif', ['FID' => 'L200', 'THM' => $tahun-(7-($i+1))], 'json');
            
            $hasil = [];
            $words =['satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh'];
            if ($getData->status) {
                $tahunSmst = 0;
                
                foreach ($getData->data as $x => $v) {
                    
                        if ($x <= 8 || $i > 4) {
                            if ($v->FKDSEM == ($v->FTHMASUK+$tahunSmst).'1' ) {
                                if (array_key_exists($tahunSmst + $i, $words)) {
                                    $hasil[$words[$tahunSmst + $i]] = $v->JML;
                                    $tahunSmst ++;
                                }
                            }
                        }                
                        else{
                            if ($v->FKDSEM == ($v->FTHMASUK+$tahunSmst).'2' ) {
                                if (array_key_exists($tahunSmst + $i, $words)) {
                                    $hasil[$words[$tahunSmst + $i]] = $v->JML;
                                    $tahunSmst ++;
                                }
                                
                            }
                        }
                                        
                    
                }
                // print_r($hasil);
                $finalResult[$i] = $hasil;
                // print_r($hasil);
                // die();
            }
            else{
                $finalResult[] = '';
            }
            
            
        }


        // print_r($finalResult);
        // die();

        // ===================== GET SELECT PRODI OPTION ====================== //
        $getProdi = $this->rest->get('prodi', 'json');
        // print_r($getProdi);
        // die();
        if($getProdi->status){
            $dprodi =[];
            foreach ($getProdi->data as $a){
                $dprodi[] = ['id' =>$a->FID, 'ket'=>$a->FProgdi];
                //$dprodi = array_merge($dprodi,$dprodi_tmp);
            }
        }


        // ======================== OUTPUT =========================== //
        $result = [
            'success' => true,
            'result'  => [
                "table_data" => [
                    'tahun-6' => $finalResult[0],
                    'tahun-5' => $finalResult[1],
                    'tahun-4' => $finalResult[2],
                    'tahun-3' => $finalResult[3],
                    'tahun-2' => $finalResult[4],
                    'tahun-1' => $finalResult[5],
                    'tahun'   => $finalResult[6]
                ],
                'prodi' => $dprodi,
                'year' => [
                    '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'
                ]
            ]
        ];

        // $this->response($result, 201);
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function filterData()
    {
        $data = ($this->input->post());
        $tahun = $data['year'];
        $prodi = $data['prodi'];
        // print_r($data);
        // die();
        // $getData = $this->rest->post('mhsAktif', ['FID' => $prodi, 'THM' => $tahun], 'json');
        // print_r($getData);
        // die();
        // ===================== GET TABLE DATA ====================== //
        $finalResult = [];
        for ($i=0; $i < 7 ; $i++) { 
            $getData = $this->rest->post('mhsAktif', ['FID' => $prodi, 'THM' => $tahun-(7-($i+1))], 'json');
            
            $hasil = [];
            $words =['satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh'];
            if ($getData->status) {
                $tahunSmst = 0;
                
                foreach ($getData->data as $x => $v) {
                    
                        if ($x <= 8) {
                            if ($v->FKDSEM == ($v->FTHMASUK+$tahunSmst).'1' ) {
                                if (array_key_exists($tahunSmst + $i, $words)) {
                                    $hasil[$words[$tahunSmst + $i]] = $v->JML;
                                    $tahunSmst ++;
                                }
                            }
                        }                
                        else{
                            if ($v->FKDSEM == ($v->FTHMASUK+$tahunSmst).'2' ) {
                                if (array_key_exists($tahunSmst + $i, $words)) {
                                    $hasil[$words[$tahunSmst + $i]] = $v->JML;
                                    $tahunSmst ++;
                                }
                                
                            }
                        }
                                        
                    
                }
                // print_r($hasil);
                $finalResult[$i] = $hasil;
                // print_r($hasil);
                // die();
            }
            else{
                $finalResult[] = '';
            }
        }
        // print_r($finalResult);
        //     die();
                
        
        $result = [
            'success' => true,
            'result'  => [
                    'tahun-6' => $finalResult[0],
                    'tahun-5' => $finalResult[1],
                    'tahun-4' => $finalResult[2],
                    'tahun-3' => $finalResult[3],
                    'tahun-2' => $finalResult[4],
                    'tahun-1' => $finalResult[5],
                    'tahun'   => $finalResult[6]
                ]
            ];
        
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function showDetailData()
    {
        $input = $this->input->post();
        $semester = $input['semester'];
        $angkatan = $input['angkatan'];
        $prodi = $input['prodi'];

        // print_r($input);
        // die();
        
        $finalResult = [];
        $getData = $this->rest->post('ListMhsAktif', ['FID' => $prodi, 'THM' => $angkatan, 'SEM' => $semester], 'json');
        
        foreach ($getData->data as $value) {
            $finalResult[] = [
                'nama' => $value->FNAMA,
                'nim'  => $value->FNIM,
                'ipk'  => $value->FIPK
            ];
        }
        /*
        Isi variabel Input
        [prodi] => All
        [angkatan] => 2012
        [semester] => 2017
        [mhs_aktif] => notacive
        */

        $result = [
            'success' => true,
            'result'  => $finalResult
        ];


        // $resultData = [];
        // for ($i=0; $i < 4000 ; $i++) { 
        //     $tmp = [
        //         'nama' => 'Aldino' . $i,
        //         'nim'  => 'L20014' . $i,
        //         'ipk'  => '3.' . $i,
        //     ];
        //     $resultData[] = $tmp;
        // };

        // $result = [
        //     'success' => true,
        //     'result'  => $resultData
        // ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function showDetailLulusan()
    {
        $input = $this->input->post();
        /*
        Isi variabel Input
        [prodi] => IF
        [angkatan] => 2012
        */

        // $result = [
        //     'success' => true,
        //     'result'  => [
        //         [
        //             'nama' => 'Aldino',
        //             'nim'  => 'L20014',
        //             'ipk'  => '3.',
        //         ],
        //         [
        //             'nama' => 'Gagak',
        //             'nim'  => 'L20014',
        //             'ipk'  => '3.',
        //         ],
        //     ]
        // ];

        $resultData = [];
        for ($i=0; $i < 200 ; $i++) { 
            $tmp = [
                'nama' => 'Fahri' . $i,
                'nim'  => 'L20014' . $i,
                'ipk'  => '3.' . $i,
            ];
            $resultData[] = $tmp;
        };

        $result = [
            'success' => true,
            'result'  => $resultData
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

}


