<?php

// use Restserver\Libraries\REST_Controller;

class Mhs_perangkatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $config = [
                'server'          => 'http://10.3.11.15/restmonitor/baa',
                'api_key'         => 'haunans',
                'api_name'        => 'X-API-KEY',
                'http_user'       => 'admin',
                'http_pass'       => '1234',
                'http_auth'       => 'basic',
                'ssl_verify_peer' => TRUE,
                'ssl_cainfo'      => '/certs/cert.pem'
            ];

        // Run some setup
        $this->rest->initialize($config);
    }


    public function initialData()
    {        
        $tahun = 2018;
        // ===================== GET TABLE DATA ====================== //
        $finalResult = [];
        for ($i=0; $i < 7 ; $i++) { 
            //$getData = $this->rest->post('mhsAktif', ['FID' => 'L200', 'THM' => $tahun-(7-($i+1))], 'json');
            
            $hasil = [];
            $words =['satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh','lulus'];
			for ($ii=0; $ii < 8 ; $ii++) { 
					if ($ii < 3) {
						$kedesem = ($tahun-(7-($i+1))+$ii).'1';
						$getData = $this->rest->post('mhsAktifTH', ['FKDSEM' => $kedesem,'FID' => '%', 'THM' => $tahun-(7-($i+1))], 'json');
						//print_r($getData);exit;
						if($getData->status){
							$hasil[$words[$ii+$i]] = $getData->data->JML;
						}
                    }
                    else{
						$kedesem = ($tahun-(7-($i+1))+$ii).'2';
						$getData = $this->rest->post('mhsAktifTH', ['FKDSEM' => $kedesem,'FID' => '%', 'THM' => $tahun-(7-($i+1))], 'json');
						if($getData->status){
							$hasil[$words[$ii+$i]] = $getData->data->JML;
						}
                    }
					if($ii == 7){
						$getData = $this->rest->post('mhsLulusTH', ['FID' => '%', 'THM' => $tahun-(7-($i+1))], 'json');
						if($getData->status){
							if($getData->data->JML > 0){
								$hasil[$words[$ii]] = $getData->data->JML;
							}else{
								$hasil[$words[$ii]] = '';
							}
						}else{
							$hasil[$words[$ii]] = '';
						}
					} 
			}
			//print_r($hasil);exit;
			$finalResult[$i] = $hasil;          
            
        }


        // print_r($finalResult);
        // die();

        // ===================== GET SELECT PRODI OPTION ====================== //
        $getProdi = $this->rest->get('prodi', 'json');
        // print_r($getProdi);
        // die();
        if($getProdi->status){
            $dprodi =[];
            foreach ($getProdi->data as $a){
                $dprodi[] = ['id' =>$a->FID, 'ket'=>$a->FProgdi];
                //$dprodi = array_merge($dprodi,$dprodi_tmp);
            }
        }


        // ======================== OUTPUT =========================== //
        $result = [
            'success' => true,
            'result'  => [
                "table_data" => [
                    'tahun-6' => $finalResult[0],
                    'tahun-5' => $finalResult[1],
                    'tahun-4' => $finalResult[2],
                    'tahun-3' => $finalResult[3],
                    'tahun-2' => $finalResult[4],
                    'tahun-1' => $finalResult[5],
                    'tahun'   => $finalResult[6]
                ],
                'prodi' => $dprodi,
                'year' => [
                    '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'
                ]
            ]
        ];

        // $this->response($result, 201);
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function filterData()
    {
        $data = ($this->input->post());
        $tahun = $data['year'];
        $prodi = $data['prodi'];
        // print_r($data);
        // die();
        // $getData = $this->rest->post('mhsAktif', ['FID' => $prodi, 'THM' => $tahun], 'json');
        // print_r($getData);
        // die();
        // ===================== GET TABLE DATA ====================== //
        $finalResult = [];
        for ($i=0; $i < 7 ; $i++) { 
            //$getData = $this->rest->post('mhsAktif', ['FID' => $prodi, 'THM' => $tahun-(7-($i+1))], 'json');
            

            $hasil = [];
            $words =['satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh','lulus'];
            //if ($getData->status) {
                $tahunSmst = 0;
                
                if ($tahun-(7-($i+1)) < 2009) {
                    $thnDataHilang = 2009 - ($tahun-(7-($i+1)));

                    // for ($loop = 0; $loop < $thnDataHilang; $loop++) { 
                    //     $hasil[$words[$loop + $i]] = '';
                        
                    // }
                    // print_r($getData);die();
                    
                    foreach ($getData->data as $v) {
                        if ($tahunSmst < 3) {
                            // print_r($v->FKDSEM);
                            // print_r($v->FTHMASUK+$tahunSmst);
                            if ($v->FKDSEM == ($v->FTHMASUK+$tahunSmst+$thnDataHilang).'1' ) {

                                if (array_key_exists($tahunSmst + $i + $thnDataHilang, $words)) {
                                    $hasil[$words[$tahunSmst + $i + $thnDataHilang]] = $v->JML;
                                    $tahunSmst ++;
                                    // print_r($tahunSmst);
                                    // die();


                                }
                            }
                            else{
                                // print_r('asdasdasd');
                            }
                        }
                        else{
                            if ($v->FKDSEM == ($v->FTHMASUK+$tahunSmst+$thnDataHilang).'2' ) {
                                if (array_key_exists($tahunSmst + $i + $thnDataHilang, $words)) {
                                    $hasil[$words[$tahunSmst + $i + $thnDataHilang]] = $v->JML;
                                    $tahunSmst ++;
                                }
                                
                            }
                        }
                    }
                    // print_r($hasil);die();
                }
                else{
                   for ($ii=0; $ii < 8 ; $ii++) { 
						if ($ii < 3) {
							$kedesem = ($tahun-(7-($i+1))+$ii).'1';
							$getData = $this->rest->post('mhsAktifTH', ['FKDSEM' => $kedesem,'FID' => $prodi, 'THM' => $tahun-(7-($i+1))], 'json');
							//print_r($getData);exit;
							if($getData->status){
								$hasil[$words[$ii+$i]] = $getData->data->JML;
							}
						}
						else{
							$kedesem = ($tahun-(7-($i+1))+$ii).'2';
							$getData = $this->rest->post('mhsAktifTH', ['FKDSEM' => $kedesem,'FID' => $prodi, 'THM' => $tahun-(7-($i+1))], 'json');
							if($getData->status){
								$hasil[$words[$ii+$i]] = $getData->data->JML;
							}
						}
						
						if($ii == 7){
							$getData = $this->rest->post('mhsLulusTH', ['FID' => $prodi, 'THM' => $tahun-(7-($i+1))], 'json');
							if($getData->status){
								if($getData->data->JML > 0){
									$hasil[$words[$ii]] = $getData->data->JML;
								}else{
									$hasil[$words[$ii]] = '';
								}
							}else{
								$hasil[$words[$ii]] = '';
							}
						} 
					}
                }
                
                // print_r($hasil);
                $finalResult[$i] = $hasil;
                // print_r($hasil);
                // die();
           // }
            //else{
            //    $finalResult[] = '';
           // }
        }
        // print_r($finalResult);
        // die();
                
        
        $result = [
            'success' => true,
            'result'  => [
                    'tahun-6' => $finalResult[0],
                    'tahun-5' => $finalResult[1],
                    'tahun-4' => $finalResult[2],
                    'tahun-3' => $finalResult[3],
                    'tahun-2' => $finalResult[4],
                    'tahun-1' => $finalResult[5],
                    'tahun'   => $finalResult[6]
                ]
            ];
        
        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

    public function showDetailData()
    {
        $input = $this->input->post();
        $semester = $input['semester'];
        $angkatan = $input['angkatan'];
        $prodi = $input['prodi'];
		$at=trim($input['mhs_aktif']);

         //echo ($at);
         //die();
        
        $finalResult = [];
        if ($at=='active') {
            $getData = $this->rest->post('ListMhsAktif', ['FID' => $prodi, 'THM' => $angkatan, 'SEM' => $semester], 'json');
        }
        else{
            $getData = $this->rest->post('ListMhsNonAktif', ['FID' => $prodi, 'THM' => $angkatan, 'SEM' => $semester], 'json');
			print_r($getData);exit;
        }
        
        
        foreach ($getData->data as $value) {
            $finalResult[] = [
                'nama' => $value->FNAMA,
                'nim'  => $value->FNIM,
                'ipk'  => $value->FIPK
            ];
        }
        /*
        Isi variabel Input
        [prodi] => All
        [angkatan] => 2012
        [semester] => 2017
        [mhs_aktif] => notacive
        */

        $result = [
            'success' => true,
            'result'  => $finalResult
        ];


        // $resultData = [];
        // for ($i=0; $i < 4000 ; $i++) { 
        //     $tmp = [
        //         'nama' => 'Aldino' . $i,
        //         'nim'  => 'L20014' . $i,
        //         'ipk'  => '3.' . $i,
        //     ];
        //     $resultData[] = $tmp;
        // };

        // $result = [
        //     'success' => true,
        //     'result'  => $resultData
        // ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function showDetailLulusan()
    {
        $input = $this->input->post();
		//print_r($input);exit;
        $prodi = $input['prodi'];
        $angkatan = $input['angkatan'];
		$getData = $this->rest->post('ListMhsLulus', ['FID' => $prodi, 'THM' => $angkatan], 'json');
		//print_r($getData);exit;
        /*
        Isi variabel Input
        [prodi] => IF
        [angkatan] => 2012
        */

        // $result = [
        //     'success' => true,
        //     'result'  => [
        //         [
        //             'nama' => 'Aldino',
        //             'nim'  => 'L20014',
        //             'ipk'  => '3.',
        //         ],
        //         [
        //             'nama' => 'Gagak',
        //             'nim'  => 'L20014',
        //             'ipk'  => '3.',
        //         ],
        //     ]
        // ];

        $resultData = [];
        foreach ($getData->data as $value) {
            $resultData[] = [
                'nama' => $value->FNAMA,
                'nim'  => $value->FNIM,
                'ipk'  => round($value->FIPK,2)
            ];
        }

        $result = [
            'success' => true,
            'result'  => $resultData
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_header(201)
            ->set_output(json_encode($result));
    }

}


