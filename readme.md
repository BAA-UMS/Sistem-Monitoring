# Sistem Monitoring Mahasiswa UMS #

## Langakah Pemakaian

1. Clone project ini
1. Setting base url sesuai folder anda
1. Buka *http://localhost/lokasi*

## Langkah Pengembangan
1. Pastikan install [NodeJS dan NPM](https://nodejs.org/en/download/) lalu lakukan ```npm install``` (karena project dikembangkan dengan syntax ES6 vue JS)
1. Masuk pada directory folder via terminal / CMD (**shift + right click** pada folder, kemudian open with **command prompt** atau **power shell**)
1. Lakukan ```npm run watch-poll``` untuk mengembangkan script vue js yang berlokasi ```application/resources/vuejs/namaFile.vue```

## Jika menggunakan htaccess
```htaccess
<IfModule mod_rewrite.c>
  RewriteEngine On
  # !IMPORTANT! Set your RewriteBase here and don't forget trailing and leading
  #  slashes.
  # If your page resides at
  #  http://www.example.com/mypage/test1
  # then use
  # RewriteBase /mypage/test1/
  RewriteBase /sistem-monitoring
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule ^(.*)$ index.php?/$1 [L]
</IfModule>

<IfModule !mod_rewrite.c>
  # If we don't have mod_rewrite installed, all 404's
  # can be sent to index.php, and everything works as normal.
  # Submitted by: ElliotHaughin

  ErrorDocument 404 /index.php
</IfModule>

```


## Jika masi terjadi error saat compile NPM
* `npm install vue` dan `npm install vue-template-compiler`
* kemudian jalankan `npm run watch-poll` atau `npm run production`


## Note
`npm run watch-poll` : untuk development  
`npm run production` : untuk produksi (minify + obfuscade)
___

## Tampilan Mahasiswa Per Angkatan
!["Mahasiswa PerAngkatan"](http://oi68.tinypic.com/2r59r0j.jpg")

## Tampilan Mahasiswa Masuk dan Lulus
!["Riwayat Pengajuan Surat"](http://oi64.tinypic.com/2u7sxad.jpg")
