const mix = require('laravel-mix');

mix.js('application/resources/vuejs/monitoring/js/monitoring.js', 'assets/js')
	.sass('application/resources/vuejs/monitoring/sass/monitoring.scss', 'assets/css')
	.setPublicPath('./');